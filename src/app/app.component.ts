import { Component } from '@angular/core'
import { environment } from 'src/environments/environment'
import { EnvironmentService } from './core/services/environment/environment.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-runtime-env';
  apiURL = environment.apiUrl;
  version = environment.version;
  constructor(private env: EnvironmentService) {
    console.log('Corriendo versión', env.version);
  }
}
