import { CommonModule } from '@angular/common'
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core'
import { throwIfAlreadyLoaded } from './module-import-guard'
import { EnvironmentServiceProvider } from './services/environment/environment.service.provider'

export const CORE_PROVIDERS = [EnvironmentServiceProvider]

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ...CORE_PROVIDERS,
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        ...CORE_PROVIDERS,
      ]
    }
  }
}
