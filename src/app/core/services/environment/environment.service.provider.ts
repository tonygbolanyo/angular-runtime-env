import { EnvironmentService } from "./environment.service"
import { EnvironmentServiceFactory } from "./environment.service.factory"

export const EnvironmentServiceProvider = {
  provide: EnvironmentService,
  useFactory: EnvironmentServiceFactory,
  deps: [],
}
