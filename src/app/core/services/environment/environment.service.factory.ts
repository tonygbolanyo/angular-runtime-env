import { IEnvironment } from "../../models/environment.interface"
import { EnvironmentService } from "./environment.service"

interface IWindow {
  [index: string]: number | string | any;
  __env?: IEnvironment;
}


export const EnvironmentServiceFactory = () => {
  let environment: IEnvironment = new EnvironmentService();

  const browserWindow:IWindow = window || {};
  if (browserWindow.hasOwnProperty('__env')) {
    const browserWindowEnv = browserWindow['__env'] || {}

    environment = { ...environment, ...browserWindowEnv }
  }
  return environment;
}
