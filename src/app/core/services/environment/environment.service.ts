import { environment } from "src/environments/environment"
import { IEnvironment } from "../../models/environment.interface"


export class EnvironmentService implements IEnvironment {
  public apiUrl = environment.apiUrl;
  public production = environment.production;
  public version = environment.version;

  constructor() {
    console.log('Environment service constructor', this)
  }
}
