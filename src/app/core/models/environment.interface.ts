export interface IEnvironment {
  [index: string]: number | string | any;
  apiUrl: string;
  production: boolean;
  version: string;
}
