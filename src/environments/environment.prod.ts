import { IEnvironment } from "src/app/core/models/environment.interface"

export const environment: IEnvironment = {
  apiUrl: window['__env']['apiUrl'] || 'http://backend.tyris.sample/api',
  production: true,
  version: '1.0.0'
};
